/**
 * Starter code for Program 3: Milestone 2. This is the header file for a promoted QTreeWidget.
 * This class handles scene graph node selection and new node creation by updating its contents as well as
 * signaling the OpenGLWidget to synchronize accordingly.  Do not modify this code.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#pragma once

#include <QTreeWidget>

class GraphManager : public QTreeWidget
{
	Q_OBJECT

public:
	GraphManager(QWidget* parent = nullptr);
	~GraphManager();

public slots:
	void changeGeometrySelection(int);
	void addNewNode(void);
	void changeNodeSelection(QTreeWidgetItem*, QTreeWidgetItem*);

signals:
	void sendSelectedNode(int);
	void addNode(int);

private:
	int currentGeometrySelection;
};
