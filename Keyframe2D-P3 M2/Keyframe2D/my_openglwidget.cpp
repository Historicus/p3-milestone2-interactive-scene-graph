/**
 * Starter code for Program 3: Milestone 2. This is the implementation file for the OpenGL widget and rendering context.
 * This file includes much of the "boilerplate" code to get the rendering context set up. Much of this assignment is filling
 * in functionality towards the bottom of the file, other comments are added for other changes which must be made.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#include "my_openglwidget.h"

My_OpenGLWidget::My_OpenGLWidget(QWidget* parent) : QOpenGLWidget(parent) {
	vertexShader = nullptr;
	fragmentShader = nullptr;
	shaderProgram = nullptr;
	sgRoot = nullptr;
	sgCurrentNode = nullptr;
	tester = new TestRender(50000);
	//initialize your geometry nodes
	//triangle: 3 points
	tri = new triangle(3);
	//square: 4 points
	sqr = new square(4);
	//trapezoid: 4 points
	trap = new trapezoid(4);
	//octagon: 8 points
	oct = new octagon(8);
	//non_convex: 18 points
	noncon = new non_convex(18);
}

My_OpenGLWidget::~My_OpenGLWidget() {
	delete vertexShader;
	delete fragmentShader;
	delete shaderProgram;
	delete sgRoot;
	delete tester;
	//delete your geometry nodes
	delete tri;
	delete sqr;
	delete trap;
	delete oct;
	delete noncon;
}

void My_OpenGLWidget::initializeGL() {
	initializeOpenGLFunctions();
	Geometry::context = this;		//let the Geometry nodes render based on this context

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//skipping the depth testing since we are using 2D graphics, all the depths would be the same
	//may need to have scene graph nodes render after their recursive call

	//allocate shaders and shader program
	vertexShader = new QOpenGLShader(QOpenGLShader::Vertex, this);
	fragmentShader = new QOpenGLShader(QOpenGLShader::Fragment, this);
	shaderProgram = new QOpenGLShaderProgram(this);

	//compile and link the shaders
	vertexShader->compileSourceFile("simple_xform_color.vert");
	fragmentShader->compileSourceFile("pass_through.frag");
	shaderProgram->addShader(vertexShader);
	shaderProgram->addShader(fragmentShader);
	shaderProgram->link();
	
	//set the shader program for use
	shaderProgram->bind();

	//get attribute and uniform locations
	Geometry::vLocation = shaderProgram->attributeLocation("vs_position");
	Geometry::cLocation = shaderProgram->attributeLocation("vs_color");
	Geometry::xformMatrixLocation = shaderProgram->uniformLocation("u_xform");

	//view matrix is handled special, not by geometry
	viewMatrixLocation = shaderProgram->uniformLocation("u_view");

	glGenBuffers(1, &Geometry::vbo);
	glGenBuffers(1, &Geometry::cbo);

	//Setting up our root node, which will have no geometry
	//this way we have a global-level transformation, akin to a camera
	sgRoot = new SceneGraph(nullptr);
	sgCurrentNode = sgRoot;	//initial selection is, in fact, the root node
}

void My_OpenGLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT);

	//traverse scene graph
	sgRoot->traverse(gMatrix4::identity());

	glFlush();
}

void My_OpenGLWidget::resizeGL(int width, int height) {
	//sets the window into the rendering context
	glViewport(0, 0, width, height);
	//hardcoded orthographic projection
	gMatrix4 ortho(gVector4(1.0f / 10.0f, 0.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 1.0f / 10.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 1.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(viewMatrixLocation, 1, GL_TRUE, reinterpret_cast<float*>(&ortho));
}

void My_OpenGLWidget::setSelectedNode(int ID) {
	//todo: update sgCurrentNode pointer so that it is pointing to the node whose ID matches the parameter
	//emit all signals based on the new selection's settings to update the GUI
	sgCurrentNode = sgRoot->findByID(ID);
	emit sendTransX(sgCurrentNode->getTranslateX());
	emit sendTransY(sgCurrentNode->getTranslateY());
	emit sendScaleX(sgCurrentNode->getScaleX());
	emit sendScaleY(sgCurrentNode->getScaleY());
	emit sendTheta(sgCurrentNode->getTheta());
}

void My_OpenGLWidget::addNewSGNode(int geomType) {
	makeCurrent();
	//todo: Add a new child to the currently selected SceneGraph node
	// geomType is from 0 to 5 and indicates nullptr, triangle, octagon, trapezoid, square, and nonconvex geometry, in that order
	Geometry* node;
	switch (geomType){
		case 0:
			node = nullptr;
			break;
		case 1:
			node = tri;
			break;
		case 2:
			node = oct;
			break;
		case 3:
			node = trap;
			break;
		case 4:
			node = sqr;
			break;
		case 5:
			node = noncon;
			break;
	}
	SceneGraph* newSG = new SceneGraph(node);
	sgCurrentNode->addChild(*newSG);
	repaint();
}

void My_OpenGLWidget::setTransX(double value) {
	//todo: change the x-translation of the currently selected node to the value given
	makeCurrent();
	sgCurrentNode->setTranslateX(value);
	repaint();
}

void My_OpenGLWidget::setTransY(double value) {
	//todo: change the y-translation of the currently selected node to the value given
	makeCurrent();
	sgCurrentNode->setTranslateY(value);
	repaint();
}

void My_OpenGLWidget::setScaleX(double value) {
	//todo: change the x-scale of the currently selected node to the value given
	makeCurrent();
	sgCurrentNode->setScaleX(value);
	repaint();
}

void My_OpenGLWidget::setScaleY(double value) {
	//todo: change the y-scale of the currently selected node to the value given
	makeCurrent();
	sgCurrentNode->setScaleY(value);
	repaint();
}

void My_OpenGLWidget::setTheta(int value) {
	//todo: change the rotation angle of the currently selected node to the value given
	makeCurrent();
	sgCurrentNode->setTheta(value);
	repaint();
}