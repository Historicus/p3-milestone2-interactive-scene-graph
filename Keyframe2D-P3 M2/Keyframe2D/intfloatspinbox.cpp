/**
 * Starter code for Program 3: Milestone 2. This is the implementation file for a promoted QDoubleSpinBox.
 * This class serves as an adapter between the sliders and spinbox itself.  Qt's sliders only work with integer
 * values, so in order to have a finer level of granularity the sliders can be given a larger range which is then
 * converted to a double by this class.  For example, translation from [-15.0, 15.0] can be represented by 
 * [-150, 150] on the corresponding slider.
 *
 * Do not modify this code.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#include "intfloatspinbox.h"

IntFloatSpinBox::IntFloatSpinBox(QWidget* parent) : QDoubleSpinBox(parent) {

}

IntFloatSpinBox::~IntFloatSpinBox() {

}

void IntFloatSpinBox::valueToConvert(int value) {
	this->setValue(value / 10.0);
}

void IntFloatSpinBox::catchChangedValue(double value) {
	emit newChanged(static_cast<int>(value * 10.0));
}
