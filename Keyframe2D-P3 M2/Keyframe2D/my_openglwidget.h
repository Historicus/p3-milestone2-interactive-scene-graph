/**
 * Starter code for Program 3: Milestone 2. This is the header file for the OpenGL widget and rendering context.
 * Necessary changes to this file are identified in comments below, as well as the assignment specification.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLShader>
#include "gVector4.h"
#include "gMatrix4.h"
#include "TestRender.h"
#include "SceneGraph.h"
//add includes for your geometry classes
#include "triangle.h"
#include "square.h"
#include "trapezoid.h"
#include "octagon.h"
#include "non_convex.h"

class My_OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	My_OpenGLWidget(QWidget* parent = nullptr);
	~My_OpenGLWidget(void);

	void initializeGL(void);
	void paintGL(void);
	void resizeGL(int, int);

public slots:
	void setSelectedNode(int);
	void addNewSGNode(int);
	void setTransX(double);
	void setTransY(double);
	void setScaleX(double);
	void setScaleY(double);
	void setTheta(int);

signals:
	void sendTransX(double);
	void sendTransY(double);
	void sendScaleX(double);
	void sendScaleY(double);
	void sendTheta(int);

private:
	QOpenGLShader* vertexShader;
	QOpenGLShader* fragmentShader;
	QOpenGLShaderProgram* shaderProgram;
	unsigned int viewMatrixLocation;
	SceneGraph* sgRoot;
	SceneGraph* sgCurrentNode;
	
	//Create pointers to your geometry classes here
	TestRender* tester;
	//shapes: subclass of geometry
	//triangle
	triangle* tri;
	//square
	square* sqr;
	//trapezoid
	trapezoid* trap;
	//octagon
	octagon* oct;
	//non-convex
	non_convex* noncon;
};