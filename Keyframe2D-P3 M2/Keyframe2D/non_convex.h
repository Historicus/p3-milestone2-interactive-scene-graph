/**
 * Header for Non-convex subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#pragma once
#include "Geometry.h"
#include "gVector4.h"

class non_convex : public Geometry
{
public:
	non_convex(unsigned int);
	~non_convex(void);
	void draw(const gMatrix4&);
	Geometry* copy() const{
		return new non_convex(*this);
	}

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

