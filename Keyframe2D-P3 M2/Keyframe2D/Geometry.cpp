/**
 * Starter code for Program 3: Milestone 2. This is the implementation file for the abstract class Geometry.
 * This file should remain unchanged.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#include "Geometry.h"

unsigned int Geometry::vLocation;
unsigned int Geometry::cLocation;
unsigned int Geometry::xformMatrixLocation;
unsigned int Geometry::vbo;
unsigned int Geometry::cbo;
QOpenGLFunctions* Geometry::context;

Geometry::Geometry() {
}

Geometry::~Geometry() {
}
