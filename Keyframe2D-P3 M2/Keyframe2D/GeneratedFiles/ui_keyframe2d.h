/********************************************************************************
** Form generated from reading UI file 'keyframe2d.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYFRAME2D_H
#define UI_KEYFRAME2D_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>
#include "graphmanager.h"
#include "intfloatspinbox.h"
#include "my_openglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_Keyframe2DClass
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    My_OpenGLWidget *openGLWidget;
    QFrame *line;
    QFrame *line_2;
    QLabel *label_2;
    GraphManager *treeWidget;
    QPushButton *pushButton;
    QComboBox *comboBox;
    QFrame *line_3;
    QGroupBox *groupBox;
    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;
    QLabel *label;
    QLabel *label_3;
    IntFloatSpinBox *doubleSpinBox_3;
    IntFloatSpinBox *doubleSpinBox_4;
    QGroupBox *groupBox_2;
    QSlider *horizontalSlider_3;
    QSlider *horizontalSlider_4;
    QLabel *label_4;
    QLabel *label_5;
    IntFloatSpinBox *doubleSpinBox;
    IntFloatSpinBox *doubleSpinBox_2;
    QGroupBox *groupBox_4;
    QSpinBox *spinBox;
    QSlider *horizontalSlider_5;
    QLabel *label_6;

    void setupUi(QMainWindow *Keyframe2DClass)
    {
        if (Keyframe2DClass->objectName().isEmpty())
            Keyframe2DClass->setObjectName(QStringLiteral("Keyframe2DClass"));
        Keyframe2DClass->resize(891, 721);
        actionExit = new QAction(Keyframe2DClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(Keyframe2DClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        openGLWidget = new My_OpenGLWidget(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        openGLWidget->setGeometry(QRect(9, 9, 550, 550));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(0, 560, 881, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(560, 10, 20, 551));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(70, 610, 871, 81));
        QFont font;
        font.setFamily(QStringLiteral("Consolas"));
        font.setPointSize(48);
        label_2->setFont(font);
        treeWidget = new GraphManager(centralWidget);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setGeometry(QRect(580, 10, 301, 251));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(580, 270, 71, 31));
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(760, 270, 121, 31));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(580, 300, 301, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(580, 320, 301, 81));
        horizontalSlider = new QSlider(groupBox);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(40, 20, 191, 22));
        horizontalSlider->setMinimum(-150);
        horizontalSlider->setMaximum(150);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider_2 = new QSlider(groupBox);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setGeometry(QRect(40, 50, 191, 22));
        horizontalSlider_2->setMinimum(-150);
        horizontalSlider_2->setMaximum(150);
        horizontalSlider_2->setOrientation(Qt::Horizontal);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 20, 21, 20));
        label->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 50, 21, 20));
        label_3->setAlignment(Qt::AlignCenter);
        doubleSpinBox_3 = new IntFloatSpinBox(groupBox);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setGeometry(QRect(240, 20, 51, 22));
        doubleSpinBox_3->setDecimals(1);
        doubleSpinBox_3->setMinimum(-15);
        doubleSpinBox_3->setMaximum(15);
        doubleSpinBox_3->setSingleStep(0.1);
        doubleSpinBox_4 = new IntFloatSpinBox(groupBox);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setGeometry(QRect(240, 50, 51, 22));
        doubleSpinBox_4->setDecimals(1);
        doubleSpinBox_4->setMinimum(-15);
        doubleSpinBox_4->setMaximum(15);
        doubleSpinBox_4->setSingleStep(0.1);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(580, 410, 301, 81));
        horizontalSlider_3 = new QSlider(groupBox_2);
        horizontalSlider_3->setObjectName(QStringLiteral("horizontalSlider_3"));
        horizontalSlider_3->setGeometry(QRect(40, 20, 191, 22));
        horizontalSlider_3->setMinimum(1);
        horizontalSlider_3->setMaximum(150);
        horizontalSlider_3->setOrientation(Qt::Horizontal);
        horizontalSlider_4 = new QSlider(groupBox_2);
        horizontalSlider_4->setObjectName(QStringLiteral("horizontalSlider_4"));
        horizontalSlider_4->setGeometry(QRect(40, 50, 191, 22));
        horizontalSlider_4->setMinimum(1);
        horizontalSlider_4->setMaximum(150);
        horizontalSlider_4->setOrientation(Qt::Horizontal);
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 20, 21, 20));
        label_4->setAlignment(Qt::AlignCenter);
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 21, 20));
        label_5->setAlignment(Qt::AlignCenter);
        doubleSpinBox = new IntFloatSpinBox(groupBox_2);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(240, 50, 51, 22));
        doubleSpinBox->setDecimals(1);
        doubleSpinBox->setMinimum(0.1);
        doubleSpinBox->setMaximum(15);
        doubleSpinBox->setSingleStep(0.1);
        doubleSpinBox_2 = new IntFloatSpinBox(groupBox_2);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setGeometry(QRect(240, 20, 51, 22));
        doubleSpinBox_2->setDecimals(1);
        doubleSpinBox_2->setMinimum(0.1);
        doubleSpinBox_2->setMaximum(15);
        doubleSpinBox_2->setSingleStep(0.1);
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(580, 500, 301, 51));
        spinBox = new QSpinBox(groupBox_4);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(240, 20, 51, 22));
        spinBox->setMaximum(359);
        horizontalSlider_5 = new QSlider(groupBox_4);
        horizontalSlider_5->setObjectName(QStringLiteral("horizontalSlider_5"));
        horizontalSlider_5->setGeometry(QRect(39, 20, 191, 22));
        horizontalSlider_5->setMaximum(359);
        horizontalSlider_5->setOrientation(Qt::Horizontal);
        label_6 = new QLabel(groupBox_4);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(9, 20, 21, 20));
        label_6->setAlignment(Qt::AlignCenter);
        Keyframe2DClass->setCentralWidget(centralWidget);

        retranslateUi(Keyframe2DClass);
        QObject::connect(comboBox, SIGNAL(currentIndexChanged(int)), treeWidget, SLOT(changeGeometrySelection(int)));
        QObject::connect(pushButton, SIGNAL(clicked()), treeWidget, SLOT(addNewNode()));
        QObject::connect(treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), treeWidget, SLOT(changeNodeSelection(QTreeWidgetItem*,QTreeWidgetItem*)));
        QObject::connect(treeWidget, SIGNAL(addNode(int)), openGLWidget, SLOT(addNewSGNode(int)));
        QObject::connect(treeWidget, SIGNAL(sendSelectedNode(int)), openGLWidget, SLOT(setSelectedNode(int)));
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), doubleSpinBox_3, SLOT(valueToConvert(int)));
        QObject::connect(doubleSpinBox_3, SIGNAL(valueChanged(double)), doubleSpinBox_3, SLOT(catchChangedValue(double)));
        QObject::connect(doubleSpinBox_3, SIGNAL(newChanged(int)), horizontalSlider, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_2, SIGNAL(valueChanged(int)), doubleSpinBox_4, SLOT(valueToConvert(int)));
        QObject::connect(doubleSpinBox_4, SIGNAL(newChanged(int)), horizontalSlider_2, SLOT(setValue(int)));
        QObject::connect(doubleSpinBox_4, SIGNAL(valueChanged(double)), doubleSpinBox_4, SLOT(catchChangedValue(double)));
        QObject::connect(horizontalSlider_3, SIGNAL(valueChanged(int)), doubleSpinBox_2, SLOT(valueToConvert(int)));
        QObject::connect(doubleSpinBox_2, SIGNAL(newChanged(int)), horizontalSlider_3, SLOT(setValue(int)));
        QObject::connect(doubleSpinBox_2, SIGNAL(valueChanged(double)), doubleSpinBox_2, SLOT(catchChangedValue(double)));
        QObject::connect(horizontalSlider_4, SIGNAL(valueChanged(int)), doubleSpinBox, SLOT(valueToConvert(int)));
        QObject::connect(doubleSpinBox, SIGNAL(newChanged(int)), horizontalSlider_4, SLOT(setValue(int)));
        QObject::connect(doubleSpinBox, SIGNAL(valueChanged(double)), doubleSpinBox, SLOT(catchChangedValue(double)));
        QObject::connect(horizontalSlider_5, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), horizontalSlider_5, SLOT(setValue(int)));
        QObject::connect(doubleSpinBox_3, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setTransX(double)));
        QObject::connect(openGLWidget, SIGNAL(sendTransX(double)), doubleSpinBox_3, SLOT(setValue(double)));
        QObject::connect(doubleSpinBox_4, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setTransY(double)));
        QObject::connect(openGLWidget, SIGNAL(sendTransY(double)), doubleSpinBox_4, SLOT(setValue(double)));
        QObject::connect(doubleSpinBox_2, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setScaleX(double)));
        QObject::connect(openGLWidget, SIGNAL(sendScaleX(double)), doubleSpinBox_2, SLOT(setValue(double)));
        QObject::connect(doubleSpinBox, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setScaleY(double)));
        QObject::connect(openGLWidget, SIGNAL(sendScaleY(double)), doubleSpinBox, SLOT(setValue(double)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), openGLWidget, SLOT(setTheta(int)));
        QObject::connect(openGLWidget, SIGNAL(sendTheta(int)), spinBox, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(Keyframe2DClass);
    } // setupUi

    void retranslateUi(QMainWindow *Keyframe2DClass)
    {
        Keyframe2DClass->setWindowTitle(QApplication::translate("Keyframe2DClass", "Keyframe2D", 0));
        actionExit->setText(QApplication::translate("Keyframe2DClass", "Exit", 0));
        label_2->setText(QApplication::translate("Keyframe2DClass", "Reserved for Finale", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("Keyframe2DClass", "Scene Graph Nodes", 0));
        pushButton->setText(QApplication::translate("Keyframe2DClass", "Add Child", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("Keyframe2DClass", "No Geometry", 0)
         << QApplication::translate("Keyframe2DClass", "Triangle", 0)
         << QApplication::translate("Keyframe2DClass", "Octagon", 0)
         << QApplication::translate("Keyframe2DClass", "Trapezoid", 0)
         << QApplication::translate("Keyframe2DClass", "Square", 0)
         << QApplication::translate("Keyframe2DClass", "Nonconvex", 0)
        );
        groupBox->setTitle(QApplication::translate("Keyframe2DClass", "Translation", 0));
        label->setText(QApplication::translate("Keyframe2DClass", "X", 0));
        label_3->setText(QApplication::translate("Keyframe2DClass", "Y", 0));
        groupBox_2->setTitle(QApplication::translate("Keyframe2DClass", "Scale", 0));
        label_4->setText(QApplication::translate("Keyframe2DClass", "X", 0));
        label_5->setText(QApplication::translate("Keyframe2DClass", "Y", 0));
        groupBox_4->setTitle(QApplication::translate("Keyframe2DClass", "GroupBox", 0));
        label_6->setText(QApplication::translate("Keyframe2DClass", "\316\270", 0));
    } // retranslateUi

};

namespace Ui {
    class Keyframe2DClass: public Ui_Keyframe2DClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYFRAME2D_H
