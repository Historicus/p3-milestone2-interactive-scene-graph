/**
 * Starter code for Program 3: Milestone 2. This is the implementation file for a promoted QTreeWidgetItem.
 * This class adds an ID for use by the GraphManager class to synchronize between the TreeView and SceneGraph.
 * Do not modify this code.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#include "sg_treeitem.h"

SG_TreeItem::SG_TreeItem(int number, int type) : QTreeWidgetItem(type) {
	ID = number;
}

SG_TreeItem::~SG_TreeItem() {

}

int SG_TreeItem::getID() {
	return ID;
}
