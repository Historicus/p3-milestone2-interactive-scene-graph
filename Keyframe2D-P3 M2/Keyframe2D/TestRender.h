/**
 * Starter code for Program 3: Milestone 2. This is the header file for the example geometry node "TestRender."
 * It serves to show how to inherit from the Geometry class, create data, and then use Geometry's static members
 * to render a scene in the OpenGL widget. Since it is an example, this file should remain unchanged.
 *
 * At this point, the TestRender geometry node is probably not very useful, but could still be used for debugging purposes.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#pragma once

#include "Geometry.h"
#include "gVector4.h"
#include "gMatrix4.h"

class TestRender : public Geometry {
public:
	TestRender(unsigned int);
	~TestRender(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

