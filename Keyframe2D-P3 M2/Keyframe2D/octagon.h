/**
 * Header for Octagon subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#pragma once
#include "Geometry.h"
#include "gVector4.h"

class octagon : public Geometry
{
public:
	octagon(unsigned int);
	~octagon(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

