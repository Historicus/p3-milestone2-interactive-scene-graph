/**
 * Starter code for Program 3: Milestone 2. This is the header file for a promoted QTreeWidgetItem.
 * This class adds an ID for use by the GraphManager class to synchronize between the TreeView and SceneGraph.
 * Do not modify this code.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#pragma once

#include <QTreeWidgetItem>

class SG_TreeItem : public QTreeWidgetItem {
public:
	SG_TreeItem(int number, int type = Type);
	~SG_TreeItem(void);
	int getID(void);
private:
	int ID;
};
