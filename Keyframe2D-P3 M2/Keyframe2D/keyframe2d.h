#ifndef KEYFRAME2D_H
#define KEYFRAME2D_H

#include <QtWidgets/QMainWindow>
#include "ui_keyframe2d.h"

class Keyframe2D : public QMainWindow
{
	Q_OBJECT

public:
	Keyframe2D(QWidget *parent = 0);
	~Keyframe2D();

private:
	Ui::Keyframe2DClass ui;
};

#endif // KEYFRAME2D_H
