/**
 * Starter code for Program 3: Milestone 2. This is the header file for the SceneGraph class. Do not change this file, this is 
 * the interface you are required to implement in the .cpp; you do not need to add any new data or functions to do so.
 *
 * You can use your SceneGraph.cpp file from the previous milestone and add new code as needed to accomplish the new tasks.
 * See the comments and assignment specification for more details.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 21, 2017
 */

#pragma once

#include <list>
#include <vector>
#include "Geometry.h"
#include "gMatrix4.h"

class SceneGraph {
public:
	SceneGraph(Geometry*);
	SceneGraph(const SceneGraph&);
	~SceneGraph(void);
	void addChild(const SceneGraph&);
	void traverse(gMatrix4) const;

	//setters and getters
	float getScaleX(void) const;
	void setScaleX(float);
	
	float getScaleY(void) const;
	void setScaleY(float);

	float getTheta(void) const;
	void setTheta(float);
	
	float getTranslateX(void) const;
	void setTranslateX(float);
	
	float getTranslateY(void) const;
	void setTranslateY(float);

	//do a depth-first search of the SceneGraph to find the node with the corresponding ID, return a pointer to that node
	SceneGraph* findByID(int);

private:
	//a master count of nodes that have been created, increment in the constructor but NOT the copy constructor
	static int nodeCount;

	std::list<SceneGraph> children;
	Geometry* geom;
	float theta;
	float scaleFactor[2];
	float translation[2];
	//this particular node's ID value
	int nodeID;
};

