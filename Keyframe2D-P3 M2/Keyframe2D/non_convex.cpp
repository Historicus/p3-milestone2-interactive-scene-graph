/**
 * cpp file for Non-convex subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#include "non_convex.h"


non_convex::non_convex(unsigned int count) : Geometry() {
	pointCount = count;
	points = new gVector4[pointCount];

	//need to triangulate for it to render
	//provide both points for each of the lines needed
	points[0] = gVector4(0,5,0,1);
	points[1] = gVector4(-1.5,2,0,1);
	points[2] = gVector4(1.5,2,0,1);

	points[3] = gVector4(1.5,2,0,1);
	points[4] = gVector4(-1.5,2,0,1);
	points[5] = gVector4(-1.5,0,0,1);

	points[6] = gVector4(-1.5,2,0,1);
	points[7] = gVector4(-4.5,1,0,1);
	points[8] = gVector4(-1.5,0,0,1);

	points[9] = gVector4(-1.5,0,0,1);
	points[10] = gVector4(1.5,2,0,1);
	points[11] = gVector4(1.5,0,0,1);

	points[12] = gVector4(1.5,0,0,1);
	points[13] = gVector4(1.5,2,0,1);
	points[14] = gVector4(4.5,1,0,1);

	points[15] = gVector4(1.5,0,0,1);
	points[16] = gVector4(-1.5,0,0,1);
	points[17] = gVector4(0,-3,0,1);

	colors = new gVector4[pointCount];
	for(int i = 0; i < pointCount; ++i) {

		colors[i] = gVector4(0.0f,0.0f,255.0f, 1.0f);
	}
}

non_convex::~non_convex() {
	delete[] points;
	delete[] colors;
}

void non_convex::draw(const gMatrix4& xform) {
	context->glUniformMatrix4fv(xformMatrixLocation, 1, GL_TRUE, reinterpret_cast<const float*>(&xform));

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), points, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), colors, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glEnableVertexAttribArray(vLocation);
	
	context->glVertexAttribPointer(vLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	
	context->glEnableVertexAttribArray(cLocation);
	context->glVertexAttribPointer(cLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	context->glDrawArrays(GL_TRIANGLES, 0, pointCount);
}
